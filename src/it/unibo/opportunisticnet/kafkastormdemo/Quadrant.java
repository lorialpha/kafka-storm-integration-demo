package it.unibo.opportunisticnet.kafkastormdemo;

import java.io.Serializable;

/**
 * Definisce un quadrante.
 * </p>
 * Un quadrante � definto da una latitudine e longitudine di partenza e una larghezza in entrambe le dimensioni. Questa classe accetta
 * latitudini e longituini tra 0 e 180 per le latitudini e tra 0 e 390 per le longitudini
 */
public class Quadrant implements Serializable {

	private double fromLatitude;
	private double fromLongitude;
	private double latitudeWidth;
	private double longitudeWidth;
	private int id;

	public Quadrant(double fromLatitude, double fromLongitude, double latitudeWidth, double longitudeWidth, int id) {
		if( fromLatitude < 0 || fromLatitude > 180.0 || fromLongitude < 0 || fromLongitude > 360) {
			throw new IllegalArgumentException( "Invalid latitude or longitude values" );
		}

		if( latitudeWidth <= 0.0 || longitudeWidth <= 0.0 || latitudeWidth >= 180.0 || longitudeWidth >= 360.0 ) {
			throw new IllegalArgumentException( "Invalid widths" );
		}

		this.fromLatitude = fromLatitude;
		this.fromLongitude = fromLongitude;
		this.latitudeWidth = latitudeWidth;
		this.longitudeWidth = longitudeWidth;
		this.id = id;
	}

	public double getStartLatitude() {
		return fromLatitude;
	}

	public double getStartLongitude() {
		return fromLongitude;
	}

	public double getEndLatitude() {
		return fromLatitude + latitudeWidth;
	}

	public double getEndLongitude() {
		if( fromLongitude + longitudeWidth > 360.0 ) {
			return fromLongitude + longitudeWidth - 360.0;
		}

		return fromLongitude + longitudeWidth;
	}

	public int getId() {
		return id;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "Quadrant{" );
		sb.append( "fromLatitude=" ).append( fromLatitude );
		sb.append( ", fromLongitude=" ).append( fromLongitude );
		sb.append( ", latitudeWidth=" ).append( latitudeWidth );
		sb.append( ", longitudeWidth=" ).append( longitudeWidth );
		sb.append( ", toLatitude=" ).append( getEndLatitude() );
		sb.append( ", toLongitude=" ).append( getEndLongitude() );
		sb.append( ", id=" ).append( id );
		sb.append( '}' );
		return sb.toString();
	}
}
