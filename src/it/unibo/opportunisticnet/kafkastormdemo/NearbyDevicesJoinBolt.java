package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Bolt che riceve le tuple emesse da {@link QuadrantNearbyDevicesEnumeratorBolt} ed emette la somma dei device vicini, anche se si
 * trovano su quadranti diversi.
 *
 * @author Lorenzo Pellegrini
 */
public class NearbyDevicesJoinBolt extends BaseRichBolt {
	private OutputCollector _collector;

	/**
	 * Mappa cos� composta:
	 * chiave esterna = id del device
	 * valore = Mappa in cui il quadrante � la chiave e il valore �...
	 * ... una mappa in cui la chiave � l'id dell'altro device, il valore la distanza
	 *
	 * somma di tutti i valori relativi a una certa chiave esterna = numero totale di device vicini
	 */
	private transient Cache<Integer, Cache<Integer, Cache<Integer,Integer>>> devicesInfo = CacheBuilder.newBuilder()
	                                                                                            .maximumSize( 10000 )
	                                                                                            .expireAfterAccess( 1, TimeUnit.MINUTES )
	                                                                                            .build();
	public NearbyDevicesJoinBolt() {
		this._collector = null;
	}

	@Override
	public void prepare( Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void cleanup() {
		for (Cache<Integer, Cache<Integer, Integer>> A : devicesInfo.asMap().values()) {
			for (Cache<Integer, Integer> B : A.asMap().values()) {
				B.invalidateAll();
				B.cleanUp();
			}

			A.invalidateAll();
			A.cleanUp();
		}

		devicesInfo.invalidateAll();
		devicesInfo.cleanUp();
		devicesInfo = null;

		super.cleanup();
	}

	@Override
	public void execute( Tuple tuple ) {
		int quadrant = tuple.getInteger( 0 );
		int deviceId = tuple.getInteger( 1 );
		int otherDeviceId = tuple.getInteger( 2 );
		int metresDistance = tuple.getInteger( 3 );
		Cache<Integer, Cache<Integer,Integer>> quadrantsInfo;
		Cache<Integer,Integer> neighborsInfo;
		int nearbyDevices = 0;

		quadrantsInfo = devicesInfo.getIfPresent( deviceId );

		if( quadrantsInfo == null ) {
			quadrantsInfo = CacheBuilder.newBuilder()
			                         .maximumSize( 10000 )
			                         .expireAfterWrite( 45, TimeUnit.SECONDS )
			                         .build();
			devicesInfo.put( deviceId, quadrantsInfo );
		}

		neighborsInfo = quadrantsInfo.getIfPresent( quadrant );

		if( neighborsInfo == null ) {
			neighborsInfo = CacheBuilder.newBuilder()
			                            .maximumSize( 10000 )
			                            .expireAfterWrite( 30, TimeUnit.SECONDS )
			                            .build();
		}

		/*
			Attenzione: quadrantsInfo.put(...) non � nell'if in quanto � comunque necessario resettare il tempo di expire
		 */
		quadrantsInfo.put( quadrant, neighborsInfo );

		neighborsInfo.put( otherDeviceId, metresDistance );

		for (Cache<Integer, Integer> nearbyDevicesAndDistances : quadrantsInfo.asMap().values()) {
			nearbyDevices+=nearbyDevicesAndDistances.size();
		}

		_collector.emit(tuple, new Values(deviceId, nearbyDevices));
		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("id", "nearby"));
	}

	private void readObject(ObjectInputStream inputStream)
	throws IOException, ClassNotFoundException
	{
		inputStream.defaultReadObject();
		devicesInfo = CacheBuilder.newBuilder()
		                              .maximumSize( 10000 )
		                              .expireAfterAccess( 1, TimeUnit.MINUTES )
		                              .build();
	}

}
