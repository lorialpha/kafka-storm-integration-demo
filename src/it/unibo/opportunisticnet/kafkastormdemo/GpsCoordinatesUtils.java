package it.unibo.opportunisticnet.kafkastormdemo;

/**
 * Classe di utility usata internamente.
 * </p>
 * Non modificare la visibilit� se non necessario
 *
 * @author Lorenzo Pellegrini
 */
class GpsCoordinatesUtils {

	private GpsCoordinatesUtils() {}

	public static double normalizeLatitude(double latitude) {
		return latitude + 90.0;
	}

	public static double normalizeLongitude(double longitude) {
		if( longitude < 0.0 ) {
			return 360.0 + longitude;
		} else {
			return longitude;
		}
	}

	/**
	 * Le coordinate sono attese come gi� normalizzate
	 *
	 * @return un array il cui primo elemento � la latitudine, il secondo la longitudine
	 */
	public static double[] translate(double latitude, double longitude, double latTranslation, double lonTranslation) {
		if( latitude < 0.0 || latitude > 180.0 || longitude < 0.0 || longitude > 360.0 ||
		    latTranslation > 180.0 || latTranslation < -180.0 || lonTranslation > 360.0 || lonTranslation < -360.0) {
			throw new IllegalArgumentException(  );
		}

		if( latitude + latTranslation < 0.0 ) {
			latitude = -(latitude+latTranslation);
			longitude= (longitude + 180.0) % 360.0;
		} else if( latitude + latTranslation > 180.0) {
			latitude = 360.0 - latitude - latTranslation;//180 - ((latitude + latTranslation) - 180.0);
			longitude= (longitude + 180.0) % 360.0;
		} else {
			latitude+=latTranslation;
		}

		if( longitude + lonTranslation > 0.0 ) {
			//Ricordarsi che il segno del risultato modulo in Java � uguale al segno del dividendo.
			//Questo if � pertanto necessario
			longitude = ( longitude + lonTranslation ) % 360.0;
		} else {
			longitude = 360.0 - ( longitude + lonTranslation );
		}

		return new double[] {latitude, longitude};
	}
}
