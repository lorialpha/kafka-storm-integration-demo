package it.unibo.opportunisticnet.kafkastormdemo;

import java.io.Serializable;
import java.util.Locale;

/**
 * Definisce una posizione sotto forma di latitudine e longitudine
 * </p>
 * La latitudine � rappresentata da un numero compreso tra -90 e +90, la longitudine tra -180 e +180
 *
 * @author Lorenzo Pellegrini
 */
public final class GpsPosition implements Serializable {

	private final double latitude;
	private final double longitude;

	public GpsPosition(double latitude, double longitude) {
		if( latitude < -90 || latitude > 90 ) {
			throw new IllegalArgumentException( "Latitude must be between -90 and 90 (" + latitude + ")" );
		}

		if( longitude < -180 || longitude > 180 ) {
			throw new IllegalArgumentException( "Longitude must be between -180 and 180 (" + longitude + ")" );
		}

		this.latitude = latitude;
		this.longitude = longitude;
	}

	public GpsPosition( GpsPosition other ) {
		this.latitude = other.latitude;
		this.longitude = other.longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public String asKafkaString() {
		return String.format( Locale.ENGLISH, "%f;%f", latitude, longitude);
	}

	public static GpsPosition fromKafkaString(String kafkaString) {
		if( kafkaString == null ) {
			throw new IllegalArgumentException(  );
		}

		String[] elements = kafkaString.split( ";" );

		if( elements.length != 2 ) {
			throw new IllegalArgumentException( "Invalid string" );
		}

		try {
			double latitude = Double.parseDouble( elements[0] );
			double longitude = Double.parseDouble( elements[1] );

			return new GpsPosition( latitude, longitude );//lancer� un'eccezione se i dati sono errati
		} catch ( NumberFormatException e ) {
			throw new IllegalArgumentException( "Invalid coordinates", e );
		}
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "GpsPosition{" );
		sb.append( "latitude=" ).append( latitude );
		sb.append( ", longitude=" ).append( longitude );
		sb.append( '}' );
		return sb.toString();
	}

	public int distanceFrom(GpsPosition other) {
		if( other == null ) {
			throw new IllegalArgumentException(  );
		}

		return (int)(1000.0 * distance( getLatitude(), getLongitude(), other.getLatitude(), other.getLongitude(), 'K' ));
	}

	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::                                                                         :*/
	/*::  This routine calculates the distance between two points (given the     :*/
	/*::  latitude/longitude of those points). It is being used to calculate     :*/
	/*::  the distance between two locations using GeoDataSource (TM) prodducts  :*/
	/*::                                                                         :*/
	/*::  Definitions:                                                           :*/
	/*::    South latitudes are negative, east longitudes are positive           :*/
	/*::                                                                         :*/
	/*::  Passed to function:                                                    :*/
	/*::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :*/
	/*::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :*/
	/*::    unit = the unit you desire for results                               :*/
	/*::          where: 'M' is statute miles (default)                         :*/
	/*::                  'K' is kilometers                                      :*/
	/*::                  'N' is nautical miles                                  :*/
	/*::  Worldwide cities and other features databases with latitude longitude  :*/
	/*::  are available at http://www.geodatasource.com                          :*/
	/*::                                                                         :*/
	/*::  For enquiries, please contact sales@geodatasource.com                  :*/
	/*::                                                                         :*/
	/*::  Official Web site: http://www.geodatasource.com                        :*/
	/*::                                                                         :*/
	/*::           GeoDataSource.com (C) All Rights Reserved 2015                :*/
	/*::                                                                         :*/
	/*::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/

	private double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60.0 * 1.1515;
		if (unit == 'K') {
			dist = dist * 1.609344;
		} else if (unit == 'N') {
			dist = dist * 0.8684;
		}
		return (dist);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts decimal degrees to radians             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	/*::  This function converts radians to decimal degrees             :*/
	/*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
	private double rad2deg(double rad) {
		return (rad * 180.0 / Math.PI);
	}
}
