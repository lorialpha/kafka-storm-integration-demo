package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * Bolt che aggiunge un campo quadrante alle tuple generate da {@link IdLatitudeLongitudeSplitterBolt}
 * </p>
 * I quadranti sono definiti staticamente indicando una serie di latitudini e longitudini. Queste andranno a formare una griglia. Ogni
 * quadrante ha un numero che lo identifica. Questo id verr� usato per smistare le tuple ai vari bolt.
 *
 * @author Lorenzo Pellegrini
 */
public class PresetGridQuadrantSplitterBolt extends BaseRichBolt {
	OutputCollector _collector;

	private NavigableMap<Double, NavigableMap<Double,Quadrant>> quadrants;

	/**
	 * Costruttore che, date le linee, numera i quadranti cos� formati
	 *
	 * @param latitudeSplits le latitudini a cui dividere il globo. Deve essere una lista di numeri crescenti da -90 a 90. Non deve
	 *                          contenere duplicati. Se il primo numero non � -90, non verr� generato un quadrante per latitudini
	 *                          inferiori a quella del primo valore. Invece verr� sempre creato il quadrante tra l'ultimo numero e +90
	 * @param longitudeSplits le longitudini a cui dividere il globo. Deve esssere una lista di numeri crescenti: prima da 0 e 180 e poi da
	 *                           -180 a 0. Non deve contenere duplicati (incluso lo 0!)
	 */
	public PresetGridQuadrantSplitterBolt( double[] latitudeSplits, double[] longitudeSplits ) {
		quadrants = new TreeMap<>(  );

		int id = 0;
		for (int i = 0; i < latitudeSplits.length; i++) {
			double latitudeSplit = latitudeSplits[i];
			double nextLatitudeSplit;
			double latitudeWidth;

			if( i == (latitudeSplits.length-1)) {
				nextLatitudeSplit = 180.0;
			} else {
				nextLatitudeSplit = latitudeSplits[i+1];
			}

			latitudeWidth = nextLatitudeSplit - latitudeSplit;

			TreeMap<Double,Quadrant> longitudesSplittedQuadrants = new TreeMap<>(  );
			quadrants.put(latitudeSplit, longitudesSplittedQuadrants);
			for (int j = 0; j < longitudeSplits.length; j++) {
				double longitudeSplit = longitudeSplits[j];
				double nextLongitudeSplit = longitudeSplits[(j+1)%longitudeSplits.length];
				double longitudeWidth;

				if(nextLongitudeSplit < longitudeSplit ) {
					longitudeWidth = nextLongitudeSplit+(360.0-longitudeSplit);
				} else {
					longitudeWidth = nextLongitudeSplit - longitudeSplit;
				}
				longitudesSplittedQuadrants.put( longitudeSplit, new Quadrant( latitudeSplit, longitudeSplit, latitudeWidth,
				                                                               longitudeWidth, id ) );
				id++;
			}
		}
	}

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute(Tuple tuple) {
		int deviceId = tuple.getInteger( 0 );
		double latitude = tuple.getDouble( 1 );
		double longitude = tuple.getDouble( 2 );

		Quadrant qu = getQuadrant( GpsCoordinatesUtils.normalizeLatitude( latitude ), GpsCoordinatesUtils.normalizeLongitude( longitude ) );

		if( qu != null ) {
			_collector.emit( tuple, new Values( qu.getId(), deviceId, latitude, longitude,
			                                    QuadrantNearbyDevicesEnumeratorBolt.ENUMERATE_AND_INDEX ) );
			_collector.ack( tuple );
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("quadrant", "id", "latitude",
		                            "longitude", "enumerationmode"));
	}

	private Quadrant getQuadrant(double latitude, double longitude) {
		Map.Entry<Double,NavigableMap<Double,Quadrant>> latitudeFiltered = quadrants.floorEntry( latitude );

		if( latitudeFiltered == null ) {
			return null;
		}

		NavigableMap<Double,Quadrant> longitudeMap = latitudeFiltered.getValue();

		Map.Entry<Double,Quadrant> longitudeQuad = longitudeMap.floorEntry( longitude );
		if( longitudeQuad == null) {
			longitudeQuad = longitudeMap.floorEntry( 360.0 );
			if( longitudeQuad == null ) {
				return null;
			}
		}

		return longitudeQuad.getValue();
	}
}