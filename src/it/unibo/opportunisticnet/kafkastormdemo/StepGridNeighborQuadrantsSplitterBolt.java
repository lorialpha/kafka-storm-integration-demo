package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.*;

/**
 * Bolt che aggiunge un campo quadrante alla richiesta in arrivo
 * </p>
 * Le tuple emesse, oltre a quella relativa all'effettivo quadrante in cui si trova il device, sono anche quelle relative agli 8
 * quadranti che confinano. Per questi quadranti viene emesso il campo "enumerationmode" pari a
 * {@link QuadrantNearbyDevicesEnumeratorBolt#ENUMERATE_NO_INDEX}. Per quello effettivo invece tale valore �
 * {@link QuadrantNearbyDevicesEnumeratorBolt#ENUMERATE_AND_INDEX}
 */
public class StepGridNeighborQuadrantsSplitterBolt extends BaseRichBolt {
	private OutputCollector _collector;
	private double latitudeStep;
	private double longitudeStep;
	private int latitudeSplitsNumber;
	private int longitudeSplitsNumber;

	public StepGridNeighborQuadrantsSplitterBolt( double latitudeStep, double longitudeStep ) {
		if( latitudeStep <= 0.0 || longitudeStep <= 0.0 ) {
			throw new IllegalArgumentException(  );
		}

		this.latitudeStep = latitudeStep;
		this.longitudeStep = longitudeStep;

		this.latitudeSplitsNumber = (int) Math.floor(180.0/latitudeStep);
		this.longitudeSplitsNumber = (int) Math.floor(360.0/longitudeStep);

		if(((double)this.latitudeSplitsNumber*latitudeStep) != 180.0) {
			this.latitudeSplitsNumber++;
		}

		if(((double)this.longitudeSplitsNumber*longitudeStep) != 360.0) {
			this.longitudeSplitsNumber++;
		}

		this._collector = null;
	}

	@Override
	public void prepare( Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute( Tuple tuple ) {
		Collection<Integer> neighbors = new HashSet<>( 9 );
		Map<Integer, String> debugMap_QuadrantToRelativePosition = new HashMap<>( 9 );
		int centralQuadrant;
		int neighborQuadrant;
		int deviceId = tuple.getInteger( 0 );
		double latitude = tuple.getDouble( 1 );
		double longitude = tuple.getDouble( 2 );

		double normalizedLatitude = GpsCoordinatesUtils.normalizeLatitude( latitude );
		double normalizedLongitude = GpsCoordinatesUtils.normalizeLongitude( longitude );

		centralQuadrant = computeQuadrantFromNormalizedCoordinates(normalizedLatitude, normalizedLongitude);

		neighbors.add(centralQuadrant);
		debugMap_QuadrantToRelativePosition.put(centralQuadrant, "Center Center");

		/*
			Visione attuale: immaginate di avere il pianeta davanti, il meridiano di greenwich davanti. Verso destra la longitudine cresce.

			Primo vicino, quello in alto a sinistra: in alto quindi +latitudeStep, a sinistra -longitudeStep
		 */
		neighborQuadrant = getNeighbouringQuadrant(centralQuadrant, 1, -1);
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put(neighborQuadrant, "Top Left");


		/**
		 * In alto alla stessa longitudine
		 */
		neighborQuadrant = getNeighbouringQuadrant(centralQuadrant, 1, 0);
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put(neighborQuadrant, "Top Center");

		/**
		 * In alto a destra
		 */
		neighborQuadrant = getNeighbouringQuadrant( centralQuadrant, 1, 1 );
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put( neighborQuadrant, "Top Right" );

		/**
		 * Alla stessa latitudine, a sinistra
		 */
		neighborQuadrant = getNeighbouringQuadrant( centralQuadrant, 0, - 1 );
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put( neighborQuadrant, "Center Left" );

		/**
		 * Alla stessa latitudine, a destra
		 */
		neighborQuadrant = getNeighbouringQuadrant( centralQuadrant, 0, 1 );
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put( neighborQuadrant, "Center Right" );

		/**
		 * Sotto, a sinistra
		 */
		neighborQuadrant = getNeighbouringQuadrant( centralQuadrant, - 1, - 1 );
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put( neighborQuadrant, "Bottom Left" );

		/**
		 * Sotto, alla stessa longitudine
		 */
		neighborQuadrant = getNeighbouringQuadrant( centralQuadrant, - 1, 0 );
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put( neighborQuadrant, "Bottom Center" );

		/**
		 * Sotto, a destra
		 */
		neighborQuadrant = getNeighbouringQuadrant( centralQuadrant, - 1, 1 );
		neighbors.add( neighborQuadrant );
		debugMap_QuadrantToRelativePosition.put(neighborQuadrant, "Bottom Right");

		for (Integer neighbor : neighbors) {
			String debug_relativePosition = debugMap_QuadrantToRelativePosition.get( neighbor );
			if( neighbor == centralQuadrant ) {
				_collector.emit( tuple, new Values( neighbor, deviceId, latitude, longitude,
				                                    QuadrantNearbyDevicesEnumeratorBolt.ENUMERATE_AND_INDEX,
				                                    debug_relativePosition ) );
			} else {
				_collector.emit( tuple, new Values( neighbor, deviceId, latitude, longitude,
				                                    QuadrantNearbyDevicesEnumeratorBolt.ENUMERATE_NO_INDEX,
				                                    debug_relativePosition ) );
			}
		}

		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("quadrant", "id", "latitude",
		                            "longitude", "enumerationmode", "debugRelativePosition"));
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "StepGridQuadrantSplitterBolt{" );
		sb.append( "latitudeStep=" ).append( latitudeStep );
		sb.append( ", longitudeStep=" ).append( longitudeStep );
		sb.append( ", latitudeSplitsNumber=" ).append( latitudeSplitsNumber );
		sb.append( ", longitudeSplitsNumber=" ).append( longitudeSplitsNumber );
		sb.append( '}' );
		return sb.toString();
	}

	private int computeQuadrantFromNormalizedCoordinates(double lat, double lon) {
		int latitudeSplit = (int) Math.floor( lat/latitudeStep );
		int longitudeSplit = (int) Math.floor( lon/longitudeStep );

		return latitudeSplit * longitudeSplitsNumber + longitudeSplit;
	}

	private int getNeighbouringQuadrant( int initialQuadrant, int latDelta, int lonDelta ) {
		int latitudeSplit = initialQuadrant / longitudeSplitsNumber;
		int longitudeSplit = initialQuadrant % longitudeSplitsNumber;

		if( latDelta > 0 ) {
			if ( latitudeSplit + 1 >= latitudeSplitsNumber ) {
				if( longitudeSplit + (longitudeSplitsNumber/2) > longitudeSplitsNumber ) {
					longitudeSplit = 360 - longitudeSplit + (longitudeSplitsNumber/2);
				} else {
					longitudeSplit = longitudeSplit + (longitudeSplitsNumber/2);
				}
			} else {
				latitudeSplit++;
			}
		} else if( latDelta < 0 ){
			if( latitudeSplit - 1 < 0 ) {
				if( longitudeSplit + (longitudeSplitsNumber/2) > longitudeSplitsNumber ) {
					longitudeSplit = 360 - longitudeSplit + (longitudeSplitsNumber/2);
				} else {
					longitudeSplit = longitudeSplit + (longitudeSplitsNumber/2);
				}
			} else {
				latitudeSplit--;
			}
		}

		if( lonDelta > 0 ) {
			if( longitudeSplit + 1 >= longitudeSplitsNumber ) {
				longitudeSplit = 0;
			} else {
				longitudeSplit++;
			}
		} else if( lonDelta < 0 ){
			if( longitudeSplit - 1 < 0 ) {
				longitudeSplit = longitudeSplitsNumber-1;
			} else {
				longitudeSplit--;
			}
		}

		return latitudeSplit*longitudeSplitsNumber + longitudeSplit;
	}
}