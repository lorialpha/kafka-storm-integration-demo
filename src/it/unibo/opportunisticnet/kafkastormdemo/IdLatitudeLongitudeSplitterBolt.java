package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;

/**
 * Semplice bolt che, ricevuta una stringa deviceid;double;double emette una tupla formata dai due double e dall'id
 * </p>
 * La tupla cos� emessa avr� il campo "latitude" uguale al primo double e il campo "longitude" uguale al secondo mentre il campo "id"
 * sar� l'intero deviceid
 *
 * @author Lorenzo Pellegrini
 */
public class IdLatitudeLongitudeSplitterBolt extends BaseRichBolt {
	OutputCollector _collector;
	private String splitPattern;

	public IdLatitudeLongitudeSplitterBolt(String splitPattern) {
		if( splitPattern == null ) {
			throw new IllegalArgumentException(  );
		}

		this.splitPattern = splitPattern;
	}

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute(Tuple tuple) {
		String value = tuple.getString( 0 );
		String[] splitValue = value.split(splitPattern);
		int id = Integer.parseInt( splitValue[0] );
		double latitude = Double.parseDouble( splitValue[1] );
		double longitude = Double.parseDouble( splitValue[2] );

		_collector.emit(tuple, new Values(id, latitude, longitude));
		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("id","latitude","longitude"));
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "IdLatitudeLongitudeSplitterBolt{" );
		sb.append( ", splitPattern='" ).append( splitPattern ).append( '\'' );
		sb.append( '}' );
		return sb.toString();
	}
}
