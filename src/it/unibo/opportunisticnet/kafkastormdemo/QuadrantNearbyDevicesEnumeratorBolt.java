package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import net.jodah.expiringmap.ExpiringMap;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Bolt che riceve una tupla da {@link QuadrantCounterBolt} e restituisce i device vicini a quello specificato
 * </p>
 * La distanza massima a cui possono trovarsi gli altri device deve essere specificata nel costruttore
 *
 * @author Lorenzo Pellegrini
 */
public class QuadrantNearbyDevicesEnumeratorBolt extends BaseRichBolt implements Serializable {
	public static final byte ENUMERATE_AND_INDEX = 0;
	public static final byte ENUMERATE_NO_INDEX = 1;


	private OutputCollector _collector;
	private int maximumDistanceMetres;

	/**
	 * Mappa cos� composta:
	 * La chiave � l'id di un quadrante.
	 * Il valore � una mappa in cui la chiave � l'id del device presente in quel quadrante, il valore � la sua posizione
	 */
	private transient Cache<Integer, Cache<Integer, GpsPosition>> quadrantDevices = CacheBuilder.newBuilder()
	                                                                                          .maximumSize( 10000 )
	                                                                                          .expireAfterAccess( 1, TimeUnit.MINUTES )
	                                                                                          .build();

	public QuadrantNearbyDevicesEnumeratorBolt( int maximumDistanceMetres ) {
		if( maximumDistanceMetres <= 0 ) {
			throw new IllegalArgumentException(  );
		}

		this.maximumDistanceMetres = maximumDistanceMetres;
		this._collector = null;
	}

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void cleanup() {
		for (Map.Entry<Integer, Cache<Integer, GpsPosition>> integerCacheEntry : quadrantDevices.asMap().entrySet()) {
			integerCacheEntry.getValue().invalidateAll();
			integerCacheEntry.getValue().cleanUp();
		}


		quadrantDevices.invalidateAll();
		quadrantDevices.cleanUp();
		quadrantDevices = null;
		super.cleanup();
	}

	@Override
	public void execute(Tuple tuple) {
		int quadrant = tuple.getInteger( 0 );
		int deviceId = tuple.getInteger( 1 );
		double latitude = tuple.getDouble( 2 );
		double longitude = tuple.getDouble( 3 );
		byte mode = tuple.getByte( 4 );

		List<Values> nearbyDevices = new LinkedList<>(  );
		GpsPosition currentDevicePosition = new GpsPosition( latitude, longitude );
		Cache<Integer, GpsPosition> devicesList = quadrantDevices.getIfPresent( quadrant );

		if( devicesList == null ) {
			devicesList = CacheBuilder.newBuilder().expireAfterWrite( 30, TimeUnit.SECONDS ).maximumSize( 10000 ).build();
			quadrantDevices.put( quadrant, devicesList );
		}

		if( mode == ENUMERATE_AND_INDEX ) {
			devicesList.put( deviceId, currentDevicePosition );
		}

		for (Map.Entry<Integer,GpsPosition> device : devicesList.asMap().entrySet()) {
			if( deviceId != device.getKey() ) {
				int distanceMetres = device.getValue().distanceFrom( currentDevicePosition );
				if ( distanceMetres <= maximumDistanceMetres ) {
					nearbyDevices.add( new Values( quadrant, deviceId, device.getKey(), distanceMetres ) );
				}
			}
		}

		for (Values nearbyDevice : nearbyDevices) {
			_collector.emit(tuple, nearbyDevice );
		}

		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("quadrant","originid", "otherid", "distance"));
	}

	private void readObject(ObjectInputStream inputStream)
	throws IOException, ClassNotFoundException
	{
		inputStream.defaultReadObject();
		quadrantDevices = CacheBuilder.newBuilder()
		                              .maximumSize( 10000 )
		                              .expireAfterAccess( 1, TimeUnit.MINUTES )
		                              .build();
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "QuadrantNearbyDevicesEnumeratorBolt{" );
		sb.append( ", maximumDistanceMetres=" ).append( maximumDistanceMetres );
		sb.append( ", quadrants=" ).append( quadrantDevices.size() ).append(" quadrants indexed");
		sb.append( '}' );
		return sb.toString();
	}
}
