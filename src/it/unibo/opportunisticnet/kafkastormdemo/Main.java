package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.StormSubmitter;
import backtype.storm.spout.SchemeAsMultiScheme;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;
import backtype.storm.utils.Utils;
import storm.kafka.*;

import java.util.UUID;

/**
 * Entry point della demo.
 * </p>
 * Viene eseguita l'inizializzazione della topologia e conseguentemente costruito il sistema di quadranti
 *
 * @author Lorenzo Pellegrini
 */
public class Main {

	private static final String BROKER_HOSTS_STRING = "127.0.0.1:2181";
	private static final String KAFKA_TOPIC_NAME = "devices_position";
	private static final String LATITUDE_LONGITUDE_SPLIT_PATTERN = ";";
	private static final double[] LATITUDE_SPLITS = { 89.8, 90.45 };
	private static final double[] LONGITUDE_SPLITS = { 0.0, 0.45 };
	private static final double LATITUDE_STEP = 0.5;
	private static final double LONGITUDE_STEP = 0.5;

	private static final int MAXIMUM_DISTANCE_BETWEEN_DEVICES = 250 * 1000;//metres

	public static void main( String[] args ) throws Exception {
		TopologyBuilder builder = new TopologyBuilder();
		/*
			Creazione dello spout che prende le tuple da Kafka
		 */
		BrokerHosts hosts = new ZkHosts( BROKER_HOSTS_STRING );
		SpoutConfig spoutConfig = new SpoutConfig( hosts, KAFKA_TOPIC_NAME, "/" + KAFKA_TOPIC_NAME, UUID.randomUUID().toString() );
		spoutConfig.scheme = new SchemeAsMultiScheme( new StringScheme() );
		KafkaSpout kafkaSpout = new KafkaSpout( spoutConfig );

		builder.setSpout( "kafka_adapter", kafkaSpout, 3 );
		builder.setBolt( "gps_split", new IdLatitudeLongitudeSplitterBolt(LATITUDE_LONGITUDE_SPLIT_PATTERN), 3 )
		       .shuffleGrouping( "kafka_adapter" );

		/*
			Quadrant split using fixed custom grid
		 */
		builder.setBolt( "quadrant_split", new PresetGridQuadrantSplitterBolt( LATITUDE_SPLITS, LONGITUDE_SPLITS ), 3 )
		       .shuffleGrouping( "gps_split" );
		builder.setBolt( "quadrant_count", new QuadrantCounterBolt(), 3 ).fieldsGrouping( "quadrant_split", new Fields( "quadrant" ) );
		builder.setBolt( "nearby_enumerator", new QuadrantNearbyDevicesEnumeratorBolt( MAXIMUM_DISTANCE_BETWEEN_DEVICES ), 3 )
		       .fieldsGrouping( "quadrant_split", new Fields( "quadrant" ) );

		/*
			Quadrant split using fixed step-created grid

			Regex log storm: Emitting: step_quadrant_split default \[
		*/
		builder.setBolt( "step_quadrant_split", new StepGridQuadrantSplitterBolt( LATITUDE_STEP, LONGITUDE_STEP ), 3 )
				.shuffleGrouping( "gps_split" );

		/*
			Quadrant split using fixed step-created grid, with neighbors tuple emitting

			Regex log storm: Emitting: step_quadrant_neighbor_split default \[
		*/
		builder.setBolt( "step_quadrant_neighbor_split", new StepGridNeighborQuadrantsSplitterBolt( LATITUDE_STEP, LONGITUDE_STEP ), 3 )
		       .shuffleGrouping( "gps_split" );

		/*
			Attach a quadrant enumerator to the step_quadrant_neighbor_split. This way bolts will also receive tuple with neighbor
			quadrant ids. Then attach a counter join to this bold. This will allow to sum up informations from all the neighbor quadrants.
		 */
		builder.setBolt( "nearby_enumerator_after_neighbor", new QuadrantNearbyDevicesEnumeratorBolt( MAXIMUM_DISTANCE_BETWEEN_DEVICES ), 3 )
		       .fieldsGrouping( "step_quadrant_neighbor_split", new Fields( "quadrant" ) );
		builder.setBolt( "total_nearby_counter_join", new NearbyDevicesJoinBolt(), 3 )
		       .fieldsGrouping( "nearby_enumerator_after_neighbor", new Fields( "originid" ) );

		Config conf = new Config();
		conf.setDebug( true );

		if ( args != null && args.length > 0 ) {
			conf.setNumWorkers( 4 );

			StormSubmitter.submitTopologyWithProgressBar( args[0], conf, builder.createTopology() );
		} else {
			LocalCluster cluster = new LocalCluster();
			cluster.submitTopology( "testgps", conf, builder.createTopology() );
			Utils.sleep( 15000 );
			cluster.killTopology( "testgps" );
			cluster.shutdown();
		}
	}

}
