package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;

/**
 * Bolt che conta il numero di contatti (nel tempo) in una certa area
 * </p>
 * Questo bolt riceve una tupla da {@link PresetGridQuadrantSplitterBolt} ed emette una tupla formata dai campi "quadrant" e "count"
 *
 * @author Lorenzo Pellegrini
 */
public class QuadrantCounterBolt extends BaseRichBolt {
	OutputCollector _collector;

	private Map<Integer, Integer> quadrantCount = new HashMap<>(  );

	public QuadrantCounterBolt() {}

	@Override
	public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute(Tuple tuple) {
		int quadrant = tuple.getInteger( 0 );

		Integer count = quadrantCount.get( quadrant );
		if( count == null ) {
			count = 0;
		}

		quadrantCount.put(quadrant, count+1);

		_collector.emit(tuple, new Values(quadrant, count+1));
		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("quadrant","count"));
	}
}
