package it.unibo.opportunisticnet.kafkastormdemo;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.util.Map;

/**
 * Bolt che aggiunge un campo quadrante alle tuple generate da {@link IdLatitudeLongitudeSplitterBolt}
 * </p>
 * I quadranti sono definiti indicando la loro larghezza in latitudine e longitudine. Queste andranno a formare una griglia.
 * Ogni quadrante ha un numero che lo identifica. Questo id verr� usato per smistare le tuple ai vari bolt.
 * </p>
 * A differenza di {@link PresetGridQuadrantSplitterBolt} non � possibile definire una griglia customizzata: � possibile creare solo griglie con
 * uno step fissato.
 *
 * @author Lorenzo Pellegrini
 */
public class StepGridQuadrantSplitterBolt extends BaseRichBolt {
	private OutputCollector _collector;
	private double latitudeStep;
	private double longitudeStep;
	private int latitudeSplitsNumber;
	private int longitudeSplitsNumber;

	public StepGridQuadrantSplitterBolt( double latitudeStep, double longitudeStep ) {
		if( latitudeStep <= 0.0 || longitudeStep <= 0.0 ) {
			throw new IllegalArgumentException(  );
		}

		this.latitudeStep = latitudeStep;
		this.longitudeStep = longitudeStep;

		this.latitudeSplitsNumber = (int) Math.floor(180.0/latitudeStep);
		this.longitudeSplitsNumber = (int) Math.floor(360.0/longitudeStep);

		if(((double)this.latitudeSplitsNumber*latitudeStep) != 180.0) {
			this.latitudeSplitsNumber++;
		}

		if(((double)this.longitudeSplitsNumber*longitudeStep) != 360.0) {
			this.longitudeSplitsNumber++;
		}
	}

	@Override
	public void prepare( Map conf, TopologyContext context, OutputCollector collector) {
		_collector = collector;
	}

	@Override
	public void execute( Tuple tuple ) {
		int deviceId = tuple.getInteger( 0 );
		double latitude = tuple.getDouble( 1 );
		double longitude = tuple.getDouble( 2 );

		double normalizedLatitude = GpsCoordinatesUtils.normalizeLatitude( latitude );
		double normalizedLongitude = GpsCoordinatesUtils.normalizeLongitude( longitude );

		int latitudeSplit = (int) Math.floor( normalizedLatitude/latitudeStep );
		int longitudeSplit = (int) Math.floor( normalizedLongitude/longitudeStep );

		int quadrant = latitudeSplit * longitudeSplitsNumber + longitudeSplit;

		_collector.emit(tuple, new Values(quadrant, deviceId, latitude, longitude));
		_collector.ack(tuple);
	}

	@Override
	public void declareOutputFields( OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("quadrant", "id", "latitude","longitude"));
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder( "StepGridQuadrantSplitterBolt{" );
		sb.append( "latitudeStep=" ).append( latitudeStep );
		sb.append( ", longitudeStep=" ).append( longitudeStep );
		sb.append( ", latitudeSplitsNumber=" ).append( latitudeSplitsNumber );
		sb.append( ", longitudeSplitsNumber=" ).append( longitudeSplitsNumber );
		sb.append( '}' );
		return sb.toString();
	}
}